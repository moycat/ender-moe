---
title: Stubs
layout: landing
description: '那些还在🦌上或已🐦掉的坑们'
image: assets/images/stubs.jpg
nav-menu: true
---

<!-- Main -->
<div id="main">

<!-- One -->
<section id="one" class="spotlights">
	<section>
		<a class="image">
			<img src="assets/images/solo-stream.jpg" alt="Solo Stream" data-position="center center" />
		</a>
		<div class="content">
			<div class="inner">
				<header class="major">
					<h3>Solo Stream</h3>
				</header>
				<p>简易的一键直播平台，可设定密码、发送弹幕。</p>
				<p>当需要直播一些奇奇怪怪的东西时，没必要向 B 站自我出道了。</p>
			</div>
		</div>
	</section>
	<section>
		<a class="image">
			<img src="assets/images/chrome-socks.jpg" alt="Solo Stream" data-position="center center" />
		</a>
		<div class="content">
			<div class="inner">
				<header class="major">
					<h3>Chrome Socks</h3>
				</header>
				<p>纯 Chrome 扩展的 Shadowsocks 客户端。</p>
				<p>挽救大陆的 Chromebook 用户于水火（暴论）。</p>
			</div>
		</div>
	</section>
</section>

<!-- Two -->
<section>
        <div class="inner">
            <header class="major">
                <h2>🕯️<s>NaïveTCP</s></h2>
            </header>
            <p>为虚拟网卡配备 <a href="https://support.microsoft.com/en-us/help/951037/information-about-the-tcp-chimney-offload-receive-side-scaling-and-net" target="_blank">TCP Chimney</a> 特性，以此在 Windows 上实现 BBR 拥塞控制算法等特性。</p>
            <blockquote>太难了，既不会 Windows 驱动开发，TCP Chimney 也被标为 deprecated 了。</blockquote>
        </div>
</section>
</div>
